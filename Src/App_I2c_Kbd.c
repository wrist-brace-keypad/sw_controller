/*
 * App_I2c_Kbd.c
 *
 *  Created on: Jun 2, 2020
 *      Author: MrGreen
 */

#include "main.h"
#include "App_I2c_Kbd.h"
#include "string.h"	//memset

void I2cKbd_Init(struct I2c_Keyboard_Struct *I2cKbd, I2C_HandleTypeDef *phi2c, uint8_t DeviceAddress8b) {
	HAL_StatusTypeDef result;
	I2cKbd->DevAddr8b = DeviceAddress8b;
	I2cKbd->phi2c = phi2c;
	I2cKbd->KeyCodeMapToUse = 0;



	result = HAL_I2C_IsDeviceReady(I2cKbd->phi2c, I2cKbd->DevAddr8b, 1, 10);
	if (result == HAL_OK) {
		HAL_GPIO_WritePin(LED_GPIO_Port, LED_Pin, GPIO_PIN_SET);
	}
	else {
		HAL_GPIO_WritePin(LED_GPIO_Port, LED_Pin, GPIO_PIN_RESET);
	}

	uint8_t dataToSend[2];

	//Configure IO Expander
	//Set All pins as inputs
	dataToSend[0] = I2cKbd_Reg_IoDir;
	dataToSend[1] = 0xFF;
	HAL_I2C_Master_Transmit(I2cKbd->phi2c, I2cKbd->DevAddr8b, dataToSend, 2, 10);

	//Enable all pullups
	dataToSend[0] = I2cKbd_Reg_GpPu;
	dataToSend[1] = 0xFF;
	HAL_I2C_Master_Transmit(I2cKbd->phi2c, I2cKbd->DevAddr8b, dataToSend, 2, 10);

	//Set all outputs low
	dataToSend[0] = I2cKbd_Reg_OLat;
	dataToSend[1] = 0x00;
	HAL_I2C_Master_Transmit(I2cKbd->phi2c, I2cKbd->DevAddr8b, dataToSend, 2, 10);


	//Configure Key Maps / Bindings

	  //Key map:
	  //	0	1	2
	  //	3	4	5	6
	  //	7	8	9	10

	//	[	w	]
	//	a	s	d	' '
	//	r	1	2	c

	//https://www.usb.org/sites/default/files/documents/hut1_12v2.pdf Pg 54
	//
	// 'a' = 0x04, ascii 97, USB table 4.  take ascii, subtract 93 to get USB table
	// '1' = 0x1E, or 1+29=30
	// '2' = 0x1F, or 2+29=31
	// '0' = 0x27 or 39
	// Return/enter = 0x28
	// Tab = 0x2B
	// Space Bar = 0x2C
	// [ = 0x2F
	// ] = 0x30
	// shift is a modifier, 0x02 or 1<<1, , code will be 0xF1.  0xF0 will get filtered out 0x01 will be the shift amount

	I2cKbd->NumOfKeys = 11;
	I2cKbd->PressReportSize = 8;


	I2cKbd->KeyMasks[0] = 0x1000;
	I2cKbd->KeyMasks[1] = 0x08;
	I2cKbd->KeyMasks[2] = 0x04;
	I2cKbd->KeyMasks[3] = 0x100;
	I2cKbd->KeyMasks[4] = 0x80;
	I2cKbd->KeyMasks[5] = 0x40;
	I2cKbd->KeyMasks[6] = 0x02;
	I2cKbd->KeyMasks[7] = 0x10;
	I2cKbd->KeyMasks[8] = 0x800;
	I2cKbd->KeyMasks[9] = 0x400;
	I2cKbd->KeyMasks[10] = 0x20;


	//default key codes / CS GO

	I2cKbd->KeyCodes[0][0] = 0x2F;		// [
	I2cKbd->KeyCodes[0][1] = 'w' - 93;	// W
	I2cKbd->KeyCodes[0][2] = 0x30;  	// ]
	I2cKbd->KeyCodes[0][3] = 'a'- 93; 	// A
	I2cKbd->KeyCodes[0][4] = 's'- 93; 	// S
	I2cKbd->KeyCodes[0][5] = 'd'- 93; 	// D
	I2cKbd->KeyCodes[0][6] = 0x2C;  	// ' '
	I2cKbd->KeyCodes[0][7] = 'r'- 93; 	// R
	I2cKbd->KeyCodes[0][8] = 1 + 29;  	// 1
	I2cKbd->KeyCodes[0][9] = 2 + 29;  	// 2
	I2cKbd->KeyCodes[0][10] ='c'-93; 	// c

	//Alt Map 1 for PSO
//	1	w	4
//	a	s	d	' '
//	5	z	e	shift
//
//
//	1 for item 1 (heal)		e for use item
//  shift for weapons
//	z for view mode
//	c for quick menu
//	x for dodge

	I2cKbd->KeyCodes[1][0] = 1 + 29;  	// 1
	I2cKbd->KeyCodes[1][1] = 'w' - 93;	// w
	I2cKbd->KeyCodes[1][2] = 4 + 29;    // 4
	I2cKbd->KeyCodes[1][3] = 'a'- 93; 	// A
	I2cKbd->KeyCodes[1][4] = 's'- 93; 	// S
	I2cKbd->KeyCodes[1][5] = 'd'- 93; 	// D
	I2cKbd->KeyCodes[1][6] = 0x2C; 		// Space
	I2cKbd->KeyCodes[1][7] = 5 + 29;  	// 5
	I2cKbd->KeyCodes[1][8] = 'z'- 93; 	// z
	I2cKbd->KeyCodes[1][9] = 'e' - 93; 	// e
	I2cKbd->KeyCodes[1][10] =0xF1;  	// shift


	uint16_t keyResult;
	I2cKbd_ReadAllKeys(I2cKbd);
	HAL_Delay(10);
	keyResult = I2cKbd_ReadAllKeys(I2cKbd);	//read twice, just in case startup glitches
	if (keyResult == I2cKbd->KeyMasks[7])  { //if bottom left key is pressed, switch map
		I2cKbd->KeyCodeMapToUse = 1;

		//Blink some LEDs
		dataToSend[0] = I2cKbd_Reg_OLat;
		dataToSend[1] = 0x00;
		HAL_I2C_Master_Transmit(I2cKbd->phi2c, I2cKbd->DevAddr8b, dataToSend, 2, 10);
		HAL_GPIO_WritePin(LED_GPIO_Port, LED_Pin, GPIO_PIN_SET);
		HAL_Delay(250);

		dataToSend[0] = I2cKbd_Reg_OLat;
		dataToSend[1] = 0x80;
		HAL_I2C_Master_Transmit(I2cKbd->phi2c, I2cKbd->DevAddr8b, dataToSend, 2, 10);
		HAL_GPIO_WritePin(LED_GPIO_Port, LED_Pin, GPIO_PIN_RESET);
		HAL_Delay(250);

		dataToSend[0] = I2cKbd_Reg_OLat;
		dataToSend[1] = 0x00;
		HAL_I2C_Master_Transmit(I2cKbd->phi2c, I2cKbd->DevAddr8b, dataToSend, 2, 10);
		HAL_GPIO_WritePin(LED_GPIO_Port, LED_Pin, GPIO_PIN_SET);
	}


}

uint16_t I2cKbd_ReadAllKeys(struct I2c_Keyboard_Struct *I2cKbd) {

	uint8_t readRowResult;

	I2cKbd->PressedKeys = 0;

	readRowResult = I2cKbd_ReadRow(I2cKbd, I2cKbd_Mask_Row0);
	I2cKbd_AddGpioToResults(I2cKbd, readRowResult);

	readRowResult = I2cKbd_ReadRow(I2cKbd, I2cKbd_Mask_Row1);
	I2cKbd_AddGpioToResults(I2cKbd, readRowResult);

	readRowResult = I2cKbd_ReadRow(I2cKbd, I2cKbd_Mask_Row2);
	I2cKbd_AddGpioToResults(I2cKbd, readRowResult);

	I2cKbd_ResultsToPressReport(I2cKbd);



	return I2cKbd->PressedKeys;

}

uint8_t I2cKbd_ReadRow (struct I2c_Keyboard_Struct *I2cKbd, uint8_t RowMask) {
	uint8_t dataToSend[2];
	uint8_t dataRxd[1];

	//Set Pins as input. Set Row as output, low.  Read port
	dataToSend[0] = I2cKbd_Reg_IoDir;
	dataToSend[1] = 0xFF ^ (RowMask | I2cKbd_Mask_LED);
	HAL_I2C_Master_Transmit(I2cKbd->phi2c, I2cKbd->DevAddr8b, dataToSend, 2, 10);

//	dataToSend[0] = I2cKbd_Reg_OLat;
//	dataToSend[1] = 0xFF ^ RowMask;
//	HAL_I2C_Master_Transmit(I2cKbd->phi2c, I2cKbd->DevAddr8b, dataToSend, 2, 10);

	//HAL_Delay(1);

	dataToSend[0] = I2cKbd_Reg_Gpio;
	HAL_I2C_Master_Transmit(I2cKbd->phi2c, I2cKbd->DevAddr8b, dataToSend, 1, 10);

	HAL_I2C_Master_Receive(I2cKbd->phi2c, I2cKbd->DevAddr8b, dataRxd, 1, 10);

	return dataRxd[0];
}

void I2cKbd_AddGpioToResults(struct I2c_Keyboard_Struct *I2cKbd, uint8_t GpioReg) {

	GpioReg = ~GpioReg;		//inver IO reg, since 0 would have meant pressed
	if (GpioReg & I2cKbd_Mask_Col0)
		I2cKbd->PressedKeys |= 1;
	I2cKbd->PressedKeys <<= 1;

	if (GpioReg & I2cKbd_Mask_Col1)
		I2cKbd->PressedKeys |= 1;
	I2cKbd->PressedKeys <<= 1;

	if (GpioReg & I2cKbd_Mask_Col2)
		I2cKbd->PressedKeys |= 1;
	I2cKbd->PressedKeys <<= 1;

	if (GpioReg & I2cKbd_Mask_Col3)
		I2cKbd->PressedKeys |= 1;
	I2cKbd->PressedKeys <<= 1;
}


void I2cKbd_ResultsToPressReport(struct I2c_Keyboard_Struct *I2cKbd) {
	memset(I2cKbd->PressReport, 0, 8);

	uint8_t pressReportLocation = 2;
	uint8_t keyCode;

	//iterate through all the key codes, but stop if the press report location becomes full
	for (uint8_t keyNum = 0; (keyNum < (I2cKbd->NumOfKeys)) && (pressReportLocation < 8); keyNum++) {
		if ((I2cKbd->PressedKeys) & (I2cKbd->KeyMasks[keyNum])) {

			keyCode = I2cKbd->KeyCodes[I2cKbd->KeyCodeMapToUse][keyNum];

			//check for standard key codes
			if (keyCode < 0xF0) {
				I2cKbd->PressReport[pressReportLocation] = keyCode;
				pressReportLocation++;
			}
			else {
				keyCode &= 0x0F;	//get rid of upper nibble
				I2cKbd->PressReport[0] |= 1 << keyCode;			//what remains in keyCode is the shift amount.  Or that with the modifier value in the press report
			}
		}

	}
}
