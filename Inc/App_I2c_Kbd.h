#include <stdint.h>

#define I2cKbd_Mask_Row0	(1 << 4)
#define I2cKbd_Mask_Row1	(1 << 5)
#define I2cKbd_Mask_Row2	(1 << 6)
#define I2cKbd_Mask_Col0	(1 << 2)
#define I2cKbd_Mask_Col1	(1 << 1)
#define I2cKbd_Mask_Col2	(1 << 3)
#define I2cKbd_Mask_Col3	(1 << 0)
#define I2cKbd_Mask_LED		(1 << 7)

#define I2cKbd_Mask_Rows_All (I2cKbd_Mask_Row0 | I2cKbd_Mask_Row1 | I2cKbd_Mask_Row2)
#define I2cKbd_Mask_Cols_All (I2cKbd_Mask_Col0 | I2cKbd_Mask_Col1 | I2cKbd_Mask_Col2 | I2cKbd_Mask_Col3)

#define I2cKbd_Reg_IoDir	0x00
#define I2cKbd_Reg_GpPu		0x06
#define I2cKbd_Reg_Gpio		0x09
#define I2cKbd_Reg_OLat		0x0A


struct I2c_Keyboard_Struct{

	I2C_HandleTypeDef *phi2c;

	uint8_t NumOfKeys;
	uint16_t KeyMasks[11];
	uint8_t KeyCodes[2][11];
	uint8_t KeyCodeMapToUse;

	uint8_t PressReportSize;
	uint8_t PressReport[8];

	uint8_t DevAddr8b;
	uint16_t PressedKeys;
};

void I2cKbd_Init(struct I2c_Keyboard_Struct *I2cKbd, I2C_HandleTypeDef *phi2c, uint8_t DeviceAddress7b);
uint16_t I2cKbd_ReadAllKeys(struct I2c_Keyboard_Struct *I2cKbd);


uint8_t I2cKbd_ReadRow (struct I2c_Keyboard_Struct *I2cKbd, uint8_t RowMask);
void I2cKbd_AddGpioToResults(struct I2c_Keyboard_Struct *I2cKbd, uint8_t GpioReg);
void I2cKbd_ResultsToPressReport(struct I2c_Keyboard_Struct *I2cKbd);
